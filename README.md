# SvServerMods

Acts as a plugin Loader for some of my mods (if the files exist)

   Workshop: [1750071149](https://steamcommunity.com/sharedfiles/filedetails/?id=1750071149)

Requires My SvModUtils Plugin [1750069108](https://steamcommunity.com/sharedfiles/filedetails/?id=1750069108)

You can also use this to run lua scripts from your ServerDir as "SvPlugins"
to do so you need to create the following directories in you server root (same as hosting.cfg)
{serverRoot} / `SvServerMods` / `SvPlugins`

create the following files in `SvPlugins` folder  

 `SvPlugins.lua` with the following content
```lua
    local plugins = {
        "TestPlugin"
    }
    return plugins;
```

`TestPlugin.lua` with the following content
```lua
    local TestPlugin = {}

    function TestPlugin.onload()
    local myLog = function(text) Log('[SvPlugin-Test]->' .. text) end
    myLog('Test Plugin Loaded Succesfully')
    return true
    end

    return TestPlugin 
```

to Add Scripts Just add the ScriptName to `SvPlugins.lua` and a new lua module with the same name
the module must follow the same layout as above and only onload gets called on plugin load and it must return true/false indicating you scripts result

You can also manualy load plugins using
    `SvMu.loadPlugin(PluginName,PluginDir)`

but you will need to run it yourself using
    `SvMu.SvPlugin.{pluginName}.Data.onload()`

you can access any table/methods contained in a loaded plugin through

 - `SvMu.SvPlugin.{pluginName}.Name` - to get its name
 - `SvMu.SvPlugin.{pluginName}.Path` - the Path it was loaded from
 - `SvMu.SvPlugin.{pluginName}.Data` - the Plugin script


When using the autoload feature a plugins failed onload method results in the plugin being immediatly unloaded
when loading a plugin and running it manualy you will need to unload it yourself..
