--[[-------------------------------------------------------------------------------------
    --  SvServerMods Loader  --
    Checks for and Loads all SvServerMods an any SvPlugins found at SvServerMods/SvPlugins

--]] -------------------------------------------------------------------------------------
SvSm = {}
SvSm.SvPluginsDir = "SvServerMods/SvPlugins"

-----------------------------------------------------------------------------------------
--Internals
-- Debugging - If Log isnt Defined then we're likely in IDE
if Log == nil then
  IsIDE = true
  -- If were in IDE SvModUtils lives Elsewhere
  SvMu = require("SvServerMods.SvModUtils.SvModUtils")
end

local myLog = function(tag, text)
  SvMu.Log("[SvSm:" .. tag .. "]" .. "->" .. text)
end

--LoadFile from Disk
local function LoadFile(fPath)
  myLog("LoadFile", "Opening: " .. fPath)
  local file = io.open(fPath)
  if file == nil then
    myLog("LoadFile", "Failed to Read file: " .. fPath)
    return nil
  end
  file:close()
  package.path = "./" .. fPath .. ";" .. package.path
  local fResult = require(fPath)
  return fResult
end
-----------------------------------------------------------------------------------------
--Class:SvPlugin - Represents a SvServerMods Plugin
local SvPlugin = {Name = "", Path = "", Data = {}}

function SvPlugin:new(PluginData)
  PluginData.parent = self
  PluginData.Data = LoadFile(PluginData.Path .. "/" .. PluginData.Name .. ".lua")
  return PluginData
end
function SvPlugin:getData()
  return self.Data
end
function SvPlugin:run(opData)
  myLog("SvPlugin:run", "Attempting to run " .. self.Name)
  local fResult = assert(self.Data.onload(opData))
  return fResult
end

local function loadPlugin(PluginName, PluginPath)
  -- Check if the PluginPath Exists
  if SvMu.isDir(PluginPath) then
    myLog("LoadPlugin", "PluginPath: " .. PluginPath .. " found..")
    --Check Plugin File Exists
    if SvMu.isFile(PluginPath .. "/" .. PluginName .. ".lua") then
      myLog("LoadPlugin", "Plugin: " .. PluginName .. " found at: " .. PluginPath .. ".lua")
      local fResult = SvPlugin:new {Name = PluginName, Path = PluginPath}
      SvSm.SvPlugin[PluginName] = fResult
    else
      myLog("LoadPlugin", "Failed to Load Plugin - File not Found")
    end
  else
    myLog("LoadPlugin", "Failed to Load Plugin - Invalid Path")
  end
end

local function LoadSvServerTools()
  if SvMu.isDir("SvServerMods/SvServerTools") then
    myLog("LoadPlugin", "SvServerTools Directory Found")
    if SvMu.isFile('SvServerMods/SvServerTools/SvServerTools.lua') then
      SvSt = {}
      local fResult = LoadFile("SvServerMods/SvServerTools/SvServerTools.lua")
      if fResult == nil then
        myLog("LoadPlugin", "Failed to Load SvServerTools")
      else
        SvSt = fResult
        SvSt.onload()
      end
    end
  end
end

local function LoadSvPlugins()
  if SvMu.isDir(SvSm.SvPluginsDir) then
    myLog("LoadSvPlugins", "SvPlugins Directory Found")
    if SvMu.isFile(SvSm.SvPluginsDir .. "/SvPlugins.lua") then
      local PluginList = LoadFile(SvSm.SvPluginsDir .. "/SvPlugins.lua")
      if PluginList == nil then
        myLog("LoadSvPlugins", "Failed to Load SvPlugins")
      else
        SvSm.SvPlugin = {}
        for i, PluginName in pairs(PluginList) do
          myLog("LoadSvPlugins", "Loading SvPlugin: " .. PluginName)
          loadPlugin(PluginName, SvSm.SvPluginsDir)
            local fResult = assert(SvSm.SvPlugin[PluginName].Data.onload())
            if fResult then
              myLog("LoadSvPlugins", PluginName .. " loaded ok")
            else
              myLog("LoadSvPlugins", PluginName .. " failed to Load")
              table.remove(SvSm.SvPlugin, i)
            end
        end
      end
    end
  end
end
-----------------------------------------------------------------------------------------
--Main Functions
--load SvServerTools if installed
LoadSvServerTools()
LoadSvPlugins()
SvSm.loadPlugin = loadPlugin
SvSm.runPlugin = function (fPlugin) SvSm.SvPlugin[fPlugin]:Run() end
--- TODO: Add ConCommands for other usable methods
